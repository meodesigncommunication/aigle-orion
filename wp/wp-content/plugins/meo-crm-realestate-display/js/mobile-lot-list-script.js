/*
    MANAGE LOT DETAILS VIEW
 */

jQuery(function(){

    jQuery('span.mobile-btn-situation').click(function(){
        openSituation(jQuery(this));
    });

    jQuery('a.btn-situation').click(function(e){
        e.preventDefault()
        openSituation(jQuery(this));
    });
    jQuery('.open-situation').click(function(e){
        e.preventDefault()
        openSituation(jQuery(this));
    });

    jQuery('span.next-lot').click(function(){

        var valid_current = 0
        var next_id = 0;
        var current_id = jQuery(this).parents('.lot-content').attr('data-id');
        var jsonToShow = $.parseJSON(jQuery('#group-lot-situation').attr('data-to-show'));

        jQuery.each(jsonToShow, function(i, item){
            if(valid_current) {
                next_id = item.id;
                valid_current = 0;
            }
            if(item.id == current_id) {
                valid_current = 1;
            }
        });

        if(next_id == 0) {
            next_id = jsonToShow[0].id;
        }

        jQuery(this).parents('.lot-content').hide();
        jQuery('#situation_'+next_id).show();

    });

    jQuery('span.previous-lot').click(function(){
        var previous_id = 0;
        var temp_previous = 0;
        var current_id = jQuery(this).parents('.lot-content').attr('data-id');
        var jsonToShow = $.parseJSON(jQuery('#group-lot-situation').attr('data-to-show'));

        jQuery.each(jsonToShow, function(i, item){
            if(item.id == current_id) {
                previous_id = temp_previous;
            }
            temp_previous = item.id;
        });

        if(previous_id == 0) {
            previous_id = jsonToShow[jsonToShow.length-1].id;
        }

        jQuery(this).parents('.lot-content').hide();
        jQuery('#situation_'+previous_id).show();
    });

});

function openSituation(element){

    jQuery(document).scrollTop(0);

    var id_show = element.attr('id-show');
    var base_height = jQuery('#list-lot-container').height();
    var data_to_show = $.parseJSON($('#group-lot-situation').attr('data-to-show'));

    if(data_to_show.length <= 1) {
        $('.lot-content aside span').css('display','none');
    }else{
        $('.lot-content aside span').css('display','block');
    }

    jQuery('#base_height').val(base_height+'px');

    jQuery('html, body').animate({
        scrollTop: $('#header').height()
    },'slow');

    element.parents('#list-lot-container').parent().find('#group-lot-situation').show();
    element.parents('#list-lot-container').parent().find('#group-lot-situation').find('#situation_'+id_show).show();

    var popup_height = element.parents('#list-lot-container').parent().find('#group-lot-situation').find('#situation_'+id_show).height();
    jQuery('#list-lot-container').height(popup_height+'px');
    element.parents('.vignette-lots').hide();
    jQuery('.wrapper-filter-list-lot').hide();

}

function closeSituation(element){
    var scrollToId = jQuery(element).attr('data-scroll-to');
    var height = jQuery('input#base_height').val();
    jQuery('.lot-content').hide();
    jQuery('.vignette-lots').show();
    jQuery('.wrapper-filter-list-lot').show();
    jQuery('#global-content-list-lot').show();
    jQuery('#group-lot-situation').hide();
    jQuery('#list-lot-container').height(height);
    jQuery(document).scrollTop(jQuery("#lot_"+scrollToId).offset().top-200);
}