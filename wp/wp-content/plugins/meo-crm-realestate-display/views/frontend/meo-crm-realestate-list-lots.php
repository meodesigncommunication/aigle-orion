<?php
/* 
 * Template name: MEO CRM REALESTATE List Lots
 */
global $wpdb;
$upload_path = wp_upload_dir();
$detect = new Mobile_Detect();
$filters = $_SESSION['filters'];
$floors = RealestateModel::selectFloor(true);
$rooms = RealestateModel::selectLotRooms();
$surfaces = RealestateModel::selectLotSurface();
$buildings = RealestateModel::selectBuildingsWithLot();
$metas = RealestateModel::selectMeta();
$filter_datas = RealestateModel::selectFilterMetaValue();

$data = array();
$data = Timber::get_context();
$post = new TimberPost();
$data['post'] = $post;
$data['posts'] = Timber::get_posts();
$data['page'] = 'Liste lots';
$data['plugin_path'] = plugins_url();
$data['type_lot'] = 'Appartements';
$data['metas'] = $metas;
$data['floors'] = $floors;
$data['rooms'] = $rooms;
$data['surfaces'] = $surfaces;
$data['surfaces_filter'] = array(
    array('label' => '25-44m<sup>2</sup>', 'value' => '25-44'),
    array('label' => '45-69m<sup>2</sup>', 'value' => '45-69'),
    array('label' => '70-94m<sup>2</sup>', 'value' => '70-94'),
    array('label' => '95-119m<sup>2</sup>', 'value' => '95-119'),
    array('label' => '&rsaquo;120m<sup>2</sup>', 'value' => '120')
);

$data['mobile_device'] = ($detect->isMobile() && !$detect->isTablet()) ? true : false ;
$data['ajaxurl'] = admin_url('admin-ajax.php');
$data['buildings'] = $buildings['buildings'];
$data['filters'] = $filters;
$data['filter_datas'] = $filter_datas;
$data['base_upload_url'] = $upload_path['baseurl'].'/';
$data['base_upload_dir'] = $upload_path['basedir'].'/';
$data['template_path'] = get_template_directory_uri();
$data['main_navigation'] = wp_nav_menu(array('menu' => 'main_navigation', 'echo' => false));
$data['page_list_lot'] = site_url('/meo-crm-realestate-building/');
$data['file_request'] = site_url('/file-request/');
$data['page_lot_details_mobile'] = site_url('/meo-crm-realestate-lot/');
$data['price_enabler'] = MeoScCf7Integration::isPriceEnabled();
$data['devise'] = MEO_CORE_DEVISE;
$data['show_price_lot'] = SHOW_PRICE_LOT; // Set in wp_config.php


// Check data filter
foreach($data['surfaces_filter'] as $key => $value){
    $check = false;
    foreach($surfaces as $surface)
    {
        list($value1, $value2) = explode('-',$value['value']); 
        
        if(isset($value1) && isset($value2))
        {
            if($value1 <= $surface->surface && $value2 >= $surface->surface)
            {
                $check = true;
            }
        }else{
            if($value['value'] <= $surface->surface)
            {
                $check = true;
            }
        }
    }
    if(!$check)
    {
        unset($data['surfaces_filter'][$key]);
    }
}

/*
 *  Include post Thumb
 */
$data['featured_image'] = (get_the_post_thumbnail( $post->ID, 'full' )) ? get_the_post_thumbnail( $post->ID, 'full' ) : '';

/*
 *  Include theme path
 */
$data['template_path'] = get_template_directory();
$data['template_path_uri'] = get_template_directory_uri();

/*
 *  Include meta Data in page
 */
$data['meta_title'] = get_field('meta_title', $post->ID);
$data['meta_description'] = get_field('meta_description', $post->ID);
$data['meta_keywords'] = get_field('meta_keywords', $post->ID);

/*
 *  Include page content in page
 */
$data['page_content'] = get_field('page_content', $post->ID);

/*
 *  Include Broker in page footer
 */
$data['broker_full_name'] = get_field('full_name', 'option');
$data['broker_phone'] = get_field('phone', 'option');
$data['broker_email'] = get_field('email', 'option');
$data['broker_picture'] = get_field('picture', 'option');
$data['broker_poste'] = get_field('poste', 'option');

/*
 *  Include other promotions in page footer
 */
$promotion_1 = get_field('promotion_1', 'option');
$promotion_2 = get_field('promotion_2', 'option');
$promotion_3 = get_field('promotion_3', 'option');

/*
 *  Include ficher header
 */
$data['info_header'] = (get_field('info_header', 'option') != 'disabled') ? true : false ;
$data['info_top_line'] = get_field('info_top_line', 'option');
$data['info_middle_line'] = get_field('info_middle_line', 'option');
$data['info_bottom_line'] = get_field('info_bottom_line', 'option');

/*
$pe_db = new wpdb(DB_USER_EXT, DB_PASSWORD_EXT, DB_NAME_EXT, DB_HOST_EXT);
$pe_db->show_errors();
*/
/*$query = " SELECT *
           FROM wp_posts AS p 
           WHERE p.ID = 1061 ";

$promotions = $pe_db->get_results($query);*/

/*
 *  Include availability apartements
 */
$data['status_popup'] = (get_field('status_popup','option') == 'enabled') ? true : false;
$data['total_disponible_popup'] = get_field('total_disponible_popup','option');

/*
 *  Include project image header
 */
$data['promo_logo'] = get_field('promo_logo','option'); // Promotion logo
$data['promo_logo_interne'] = get_field('promo-logo-interne','option'); // Promotion logo
$data['status_img_header'] = (get_field('status_img_header','option') == 'enabled') ? true : false;
$data['image_header'] = get_field('image_header','option');

Timber::render('twig/meo-crm-realestate-list-lots.html.twig', $data);