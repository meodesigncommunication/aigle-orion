<?php
/* 
 * Template name: MEO CRM REALESTATE Building
 */
$detect = new Mobile_Detect();
$buildings = RealestateModel::selectBuildingsWithLot();
$metas = RealestateModel::selectMeta();
$upload_path = wp_upload_dir();

$data = array();

$data = Timber::get_context();
$data['posts'] = Timber::get_posts();
$data['page'] = 'Building';
$data['plugin_path'] = plugins_url();
$data['type_lot'] = 'Appartements';
$data['metas'] = $metas;
$data['buildings'] = $buildings['buildings'];
$data['building_selected'] = (isset($_GET['id']) && !empty($_GET['id'])) ? $_GET['id'] : 0 ;
$data['base_upload_url'] = $upload_path['baseurl'].'/';
$data['base_upload_dir'] = $upload_path['basedir'].'/';
$data['template_path'] = get_template_directory_uri();
$data['main_navigation'] = wp_nav_menu(array('menu' => 'main_navigation', 'echo' => false));
$data['page_list_lot'] = site_url('/meo-crm-realestate-list-lots/');
$data['file_request'] = site_url('/file-request/');
$data['price_enabler'] = MeoScCf7Integration::isPriceEnabled();
$data['devise'] = MEO_CORE_DEVISE;
$data['show_price_lot'] = SHOW_PRICE_LOT; // Set in wp_config.php

/*
 *  Include post Thumb
 */
$data['featured_image'] = (get_the_post_thumbnail( $post->ID, 'full' )) ? get_the_post_thumbnail( $post->ID, 'full' ) : '';

/*
 *  Include theme path
 */
$data['template_path'] = get_template_directory();
$data['template_path_uri'] = get_template_directory_uri();

/*
 *  Include meta Data in page
 */
$data['meta_title'] = get_field('meta_title', $post->ID);
$data['meta_description'] = get_field('meta_description', $post->ID);
$data['meta_keywords'] = get_field('meta_keywords', $post->ID);

/*
 *  Include page content in page
 */
$data['page_content'] = get_field('page_content', $post->ID);

/*
 *  Include Broker in page footer
 */
$data['broker_full_name'] = get_field('full_name', 'option');
$data['broker_phone'] = get_field('phone', 'option');
$data['broker_email'] = get_field('email', 'option');
$data['broker_picture'] = get_field('picture', 'option');
$data['broker_poste'] = get_field('poste', 'option');

/*
 *  Include other promotions in page footer
 */
$promotion_1 = get_field('promotion_1', 'option');
$promotion_2 = get_field('promotion_2', 'option');
$promotion_3 = get_field('promotion_3', 'option');
/*
$pe_db = new wpdb(DB_USER_EXT, DB_PASSWORD_EXT, DB_NAME_EXT, DB_HOST_EXT);
$pe_db->show_errors();
*/
/*$query = " SELECT *
           FROM wp_posts AS p 
           WHERE p.ID = 1061 ";

$promotions = $pe_db->get_results($query);*/

/*
 *  Include availability apartements
 */
$data['status_popup'] = (get_field('status_popup','option') == 'enabled') ? true : false;
$data['total_disponible_popup'] = get_field('total_disponible_popup','option');

/*
 *  Include project image header
 */
$data['promo_logo'] = get_field('promo_logo','option'); // Promotion logo
$data['promo_logo_interne'] = get_field('promo-logo-interne','option'); // Promotion logo
$data['status_img_header'] = (get_field('status_img_header','option') == 'enabled') ? true : false;
$data['image_header'] = get_field('image_header','option');

if($detect->isMobile() && !$detect->isTablet())
{
    $data['smartphone'] = true;
}else{
    $data['smartphone'] = false;
}

//echo 'ok'; exit();

if($detect->isMobile() && !$detect->isTablet())
{
    Timber::render('twig/meo-crm-realestate-building-mobile.html.twig', $data);
}else{
    Timber::render('twig/meo-crm-realestate-building.html.twig', $data);    
}