<?php
/* 
 * Template name: MEO CRM REALESTATE Sector
 */

//Init variables
global $wpdb;
$detect = new Mobile_Detect();
$sector_id = $_GET['id'];
$upload_path = wp_upload_dir();

// Select selected sector
$wheres = array( array('key' => 'id', 'value' => $sector_id, 'before' => '', 'comparator' => '=')); // Array for create a where in query 
$sector = RealestateModel::selectSectorWhere($wheres); // Execute query and return datas

// Select building linked to sector
$wheres = array(array('key' => 'sector_id', 'value' => $sector_id, 'before' => '', 'comparator' => '=')); // Array for create a where in query 
$buildings = RealestateModel::selectBuildingWhere($wheres); // Execute query and return datas


// Init twig variables
$data = Timber::get_context();
$post = new TimberPost();
$data['post'] = $post;
$data['posts'] = Timber::get_posts();
$data['sector'] = $sector[0];
$data['buildings'] = $buildings;
$data['page'] = 'Sector';
$data['base_upload_url'] = $upload_path['baseurl'].'/';
$data['base_upload_dir'] = $upload_path['basedir'].'/';
$data['template_path'] = get_template_directory_uri();
$data['main_navigation'] = wp_nav_menu(array('menu' => 'main_navigation', 'echo' => false));
$data['is_mobile'] = ($detect->isMobile() || $detect->isTablet()) ? true : false;

/*
 *  Include post Thumb
 */
$data['featured_image'] = (get_the_post_thumbnail( $post->ID, 'full' )) ? get_the_post_thumbnail( $post->ID, 'full' ) : '';

/*
 *  Include theme path
 */
$data['template_path'] = get_template_directory();
$data['template_path_uri'] = get_template_directory_uri();

/*
 *  Include meta Data in page
 */
$data['meta_title'] = get_field('meta_title', $post->ID);
$data['meta_description'] = get_field('meta_description', $post->ID);
$data['meta_keywords'] = get_field('meta_keywords', $post->ID);

/*
 *  Include page content in page
 */
$data['page_content'] = get_field('page_content', $post->ID);

/*
 *  Include Broker in page footer
 */
$data['broker_full_name'] = get_field('full_name', 'option');
$data['broker_phone'] = get_field('phone', 'option');
$data['broker_email'] = get_field('email', 'option');
$data['broker_picture'] = get_field('picture', 'option');
$data['broker_poste'] = get_field('poste', 'option');

/*
 *  Include other promotions in page footer
 */
$promotion_1 = get_field('promotion_1', 'option');
$promotion_2 = get_field('promotion_2', 'option');
$promotion_3 = get_field('promotion_3', 'option');
/*
$pe_db = new wpdb(DB_USER_EXT, DB_PASSWORD_EXT, DB_NAME_EXT, DB_HOST_EXT);
$pe_db->show_errors();
*/
/*$query = " SELECT *
           FROM wp_posts AS p
           WHERE p.ID = 1061 ";

$promotions = $pe_db->get_results($query);*/

/*
 *  Include availability apartements
 */
$data['status_popup'] = (get_field('status_popup','option') == 'enabled') ? true : false;
$data['total_disponible_popup'] = get_field('total_disponible_popup','option');

/*
 *  Include project image header
 */
$data['promo_logo'] = get_field('promo_logo','option'); // Promotion logo
$data['promo_logo_interne'] = get_field('promo-logo-interne','option'); // Promotion logo
$data['status_img_header'] = (get_field('status_img_header','option') == 'enabled') ? true : false;
$data['image_header'] = get_field('image_header','option');

// Show a twig template page
Timber::render('twig/meo-crm-realestate-sector.html.twig', $data);