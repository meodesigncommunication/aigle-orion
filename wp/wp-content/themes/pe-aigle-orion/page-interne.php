<?php
/*
 * Template name: TPL Page interne
 */

require_once 'include/base.php';

Timber::render( 'templates/layouts/header-page-interne.html.twig' , $context );
Timber::render( 'templates/pageinterne.html.twig' , $context );
Timber::render( 'templates/layouts/footer.html.twig' , $context );