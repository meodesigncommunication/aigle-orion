$(document).ready(function(){
    //Nav switcher for mobile devices
    $('#nav-icon3').click(function(){
        //Toggle hamburger menu - btn css - nav open
        $(this).toggleClass('open');
        $(".phone-burger").toggleClass('open-switch');
        $("nav").toggleClass("open-menu");
        $(".header-home-page, .header-page-interne").toggleClass("fullHeight");
    });
    //Call the slider
    var swiper = new Swiper('.swiper-container', {
        autoplay: 2800,
        speed: 1000,
        effect: "fade",
        pagination: '.swiper-pagination',
        paginationClickable: true
    });
    //Call fancybox
    $('.fancybox-smartcapture').fancybox({
        'width': 415,
        'height': 800,
        'autoDimensions': false,
        'type': 'iframe',
        'autoSize': false
    });
});