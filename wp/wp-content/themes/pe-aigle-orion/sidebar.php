<?php
/**
 * The Template for displaying all single posts
 *
 *
 * @package  WordPress
 * @subpackage  Timber
 */

Timber::render( array( 'sidebar.twig' ), $data ); ?>

<div class="wrap-content-table">
    <div class="cell-col col-66"></div>
    <div class="cell-col col-33"></div>
</div>
