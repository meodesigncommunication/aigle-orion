<?php
/* 
 * Template name: MEO CRM REALESTATE Developpement
 */

global $wpdb;

$upload_path = wp_upload_dir();

// GET DEVELOPPEMENT
$developpements = RealestateModel::selectDeveloppement();

// GET SECTOR
$sectors = RealestateModel::selectSector();

// DECLARE TWIG VARIABLES
$data = Timber::get_context();
$data['developpements'] = $developpements;
$data['sectors'] = $sectors;
$data['page'] = 'Developpement';
$data['base_upload_url'] = $upload_path['baseurl'].'/';
$data['base_upload_dir'] = $upload_path['basedir'].'/';
$data['template_path'] = get_template_directory_uri();
$data['main_navigation'] = wp_nav_menu(array('menu' => 'main_navigation', 'echo' => false));

Timber::render('twig/meo-crm-realestate-developpement.html.twig', $data);