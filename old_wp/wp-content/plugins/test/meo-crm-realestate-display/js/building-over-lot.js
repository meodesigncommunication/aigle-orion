jQuery(document).ready(function(){
    
   /* building front image map */
    jQuery('.building_map area').mouseover(function(){
        var id = jQuery(this).attr('data-id');
        jQuery('.status_lot_hover_'+id).each(function(){
            jQuery(this).css('display','block');
        }); 
        jQuery('.status_lot_'+id).each(function(){
            jQuery(this).css('display','none');
        });    
    });
    jQuery('.building_map area').mouseout(function(){
        var id = jQuery(this).attr('data-id');
        jQuery('.status_lot_hover_'+id).each(function(){
            jQuery(this).css('display','none');
        }); 
        jQuery('.status_lot_'+id).each(function(){
            jQuery(this).css('display','block');
        }); 
    });

    /* building back image map */
    jQuery('.building_map area').mouseover(function(){
        var id = jQuery(this).attr('data-id');
        jQuery('.status_lot_hover_back_'+id).each(function(){
            jQuery(this).css('display','block');
        }); 
        jQuery('.status_lot_back_'+id).each(function(){
            jQuery(this).css('display','none');
        }); 
    });
    jQuery('.building_map area').mouseout(function(){
        var id = jQuery(this).attr('data-id');        
        jQuery('.status_lot_hover_back_'+id).each(function(){
            jQuery(this).css('display','none');
        }); 
        jQuery('.status_lot_back_'+id).each(function(){
            jQuery(this).css('display','block');
        }); 
    }); 
    
});