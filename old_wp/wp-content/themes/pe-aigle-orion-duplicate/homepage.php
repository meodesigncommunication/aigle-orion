<?php
/*
 * Template name: TPL Homepage
 */

global $wpdb;

$post = new TimberPost();
$context = Timber::get_context();
$context['post'] = $post;
$context['galleries'] = array();
$context['template_path'] = get_template_directory();
$context['template_path_uri'] = get_template_directory_uri();
$context['slider'] = (have_rows('slider')) ? get_field('slider') : array();
$context['url_plan'] = site_url( '/list-lots/' );
$context['url_contact'] = site_url( '/contact/' );
$context['text_courtier_footer'] = 'Vous pouvez contacter notre conseiller directement par téléphone.';
$context['navigation_home'] = wp_nav_menu(array('menu' => 'homepage_nav', 'echo' => false));

$query = 'SELECT * 
          FROM wp_posts as p
          INNER JOIN wp_postmeta as pm ON pm.post_id = p.ID
          WHERE p.post_type = "realestate_broker" 
          ORDER BY RAND()
          LIMIT 1';

$courtiers = $wpdb->get_results($query);
$courtier = $courtiers[0];

if( have_rows('gallery') ):
    while( have_rows('gallery') ): the_row();
        $context['galleries'][] =  get_sub_field('image');
    endwhile;
endif;

$context['default_background'] = $context['galleries'][0]['url'];

$context['courtier']['full_name'] = get_field('full_name',$courtier->ID);
$context['courtier']['phone'] = get_field('phone',$courtier->ID);
$context['courtier']['email'] = get_field('email',$courtier->ID);
$context['courtier']['picture'] = get_field('picture',$courtier->ID);

Timber::render( 'templates/homepage.twig' , $context );