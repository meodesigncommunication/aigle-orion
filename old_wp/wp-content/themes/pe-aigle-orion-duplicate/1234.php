<!--:en-->
    <p>Optima Energy is active in the trading of crude oil, refined oil products and specialised products such as base oils and bitumen, as well as related investment activities. Our primary spheres of operation are Europe, Africa and the Americas. Based on strategic alliances with local traders and excellent relations with national oil companies, consumers and producers, the group became a significant trader of oil products in West Africa. Developing asset-based infrastructure in Africa is one of the key strategies of The Optima Energy Group. Optima Energy Group Ltd, the holding company, is based in Cyprus. Other offices, including the trading hub in Geneva, are strategically placed throughout Europe and Africa. In addition the company has representatives in New Jersey and Puerto Rico, coordinating activities in North, South and Central America. <a href="/wp/wp-content/uploads/2017/03/2235_Optima_siteweb_AboutUs-map-meo20170327.jpg"><img class="alignnone size-medium wp-image-1108" src="/wp/wp-content/uploads/2017/03/2235_Optima_siteweb_AboutUs-map-meo20170327.jpg" alt="2235_Optima_siteweb_AboutUs-map-meo20170327" /></a></p>
    <div style="width: 100%; display: flex;">
        <div style="width: 10%; flex: auto;"><a href="/wp/wp-content/uploads/2017/02/photo-ADA-1.jpg"><img class="alignnone size-thumbnail wp-image-1079" style="width: 80%; margin: 0 auto;" src="/wp/wp-content/uploads/2017/02/photo-ADA-1.jpg" alt="photo ADA 1" /></a>
            <p style="text-align: center; font-size: 12px;">Bitumen terminal<br>CANARY<br>ISLANDS</p>
        </div>
        <div style="width: 10%; flex: auto;"><a href="/wp/wp-content/uploads/2013/08/tradingAfrica_PROP2.jpg"><img class="alignnone size-thumbnail wp-image-688" style="width: 80%; margin: 0 auto;" src="/wp/wp-content/uploads/2013/08/tradingAfrica_PROP2.jpg" alt="tradingAfrica_PROP2" /></a>
            <p style="text-align: center; font-size: 12px;">Filling station<br>network<br>SENEGAL</p>
        </div>
        <div style="width: 10%; flex: auto;">
            <p style="text-align: center;"><a href="/wp/wp-content/uploads/2017/04/img_nigeria_station.jpg"><img class="alignnone size-thumbnail wp-image-1082" style="width: 80%; margin: 0 auto;" src="/wp/wp-content/uploads/2017/04/img_nigeria_station.jpg" alt="photo Cameroun" /></a></p>
            <p style="text-align: center; font-size: 12px;">Filling station<br>network<br>NIGERIA</p>
        </div>
        <div style="width: 10%; flex: auto;"><a href="/wp/wp-content/uploads/2013/08/TradingEurope_PROP3.jpg"><img class="alignnone size-thumbnail wp-image-627" style="width: 80%; margin: 0 auto;" src="/wp/wp-content/uploads/2013/08/TradingEurope_PROP3.jpg" alt="Trading Europe" /></a>
            <p style="text-align: center; font-size: 12px;">Clean products<br>terminal<br>NIGERIA</p>
        </div>
        <div style="width: 10%; flex: auto;"><a href="/wp/wp-content//uploads/2017/02/photo-Cameroun.jpg"><img class="alignnone size-thumbnail wp-image-1082" style="width: 80%; margin: 0 auto;" src="/wp/wp-content/uploads/2017/02/photo-Cameroun.jpg" alt="photo Cameroun" /></a>
            <p style="text-align: center; font-size: 12px;">Filling station<br>network<br>CAMEROUN</p>
            <p style="text-align: center;">&nbsp;</p>
        </div>
    </div>
<!--:-->